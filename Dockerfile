FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

ARG VERSION=3.5.4

RUN mkdir -p /app/pkg /app/data /run/ycache /run/yarn /run/cache /app/pkg/cloudron/.cache /run/tmp
WORKDIR /app/pkg

#install yarn
RUN npm install --global yarn

#set cache directory and install strapi quickstart
RUN yarn config set cache-folder /run/yarn && \
    yarn create strapi-app cloudron --quickstart --no-run

WORKDIR /app/pkg/cloudron

#install postgres, smtp and slugify && strapi modules
RUN yarn add pg slugify strapi-provider-email-smtp && \
    yarn strapi install email documentation graphql

#build
RUN yarn build --clean

#add database and plugins config
COPY database.js /app/pkg/database.js
COPY plugins.js /app/pkg/plugins.js

#move config directory and link to it
RUN mv /app/pkg/cloudron/config /app/pkg/cloudron/config_orig && \
    ln -s /app/data/config /app/pkg/cloudron/config

#remove old dirs and link to new ones
RUN rm -rf /app/pkg/cloudron/.cache \
           /app/pkg/cloudron/.tmp \
           /app/pkg/cloudron/build \
           /app/pkg/cloudron/components \
           /app/pkg/cloudron/public/uploads \
           /app/pkg/cloudron/plugin \
           /app/pkg/cloudron/api \
           /app/pkg/cloudron/extensions

RUN ln -s /app/data/plugin /app/pkg/cloudron/plugin && \
    ln -s /app/data/api /app/pkg/cloudron/api && \
    ln -s /app/data/extensions /app/pkg/cloudron/extensions && \
    ln -s /app/data/uploads /app/pkg/cloudron/public/uploads && \
    ln -s /app/data/components /app/pkg/cloudron/components && \
    ln -s /app/data/exports /app/pkg/cloudron/exports

#this needs cleanup, went a bit overboard while testing (node/yarn tried to write in many directories)
RUN ln -s /run/yarn /home/cloudron/.yarn && \
    ln -s /run/cache /app/pkg/cloudron/.cache && \
    ln -s /run/ycache /home/cloudron/.cache && \
    ln -s /run/tmp /app/pkg/cloudron/.tmp && \
    rm -rf /usr/local/share/.yarnrc && \
    ln -s /run/yarnrc /usr/local/share/.yarnrc && \
    ln -s /app/data/build /app/pkg/cloudron/build && \
    rm -rf /usr/local/share/.cache/yarn && \
    ln -s /app/data/cache /usr/local/share/.cache/yarn && \
    rm -rf /usr/local/share/.config/yarn && \
    ln -s /app/data/yarnconfig /usr/local/share/.config/yarn && \
    ln -s /app/data/rc /home/cloudron/.yarnrc && \
    rm -rf /usr/local/share/.yarn && ln -s /app/data/yarn /usr/local/share/.yarn && \
    ln -s /run/yarn-error.log /app/pkg/cloudron/yarn-error.log

RUN chown -R cloudron.cloudron /app/pkg /run /home/cloudron /app/data

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
