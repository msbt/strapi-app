This app packages Strapi <upstream>3.1.6</upstream>

### About

Strapi is a flexible, open-source Headless CMS that gives developers the freedom to choose their favorite tools and frameworks while also allowing editors to easily manage and distribute their content.

- [Strapi Git](https://github.com/strapi/strapi)

