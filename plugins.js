module.exports = ({ env }) => ({
  email: {
    provider: 'smtp',
    providerOptions: {
      host: 'CLOUDRON_MAIL_SMTP_SERVER',
      port: 'CLOUDRON_MAIL_SMTP_PORT',
      secure: false,
      username: 'CLOUDRON_MAIL_SMTP_USERNAME',
      password: 'CLOUDRON_MAIL_SMTP_PASSWORD',
      rejectUnauthorized: true,
      requireTLS: false,
      connectionTimeout: 1,
    },
    settings: {
      defaultFrom: 'CLOUDRON_MAIL_FROM',
      defaultReplyTo: 'CLOUDRON_MAIL_FROM',
      from: 'CLOUDRON_MAIL_FROM',
      replyTo: 'CLOUDRON_MAIL_FROM',
    },
  },
});
