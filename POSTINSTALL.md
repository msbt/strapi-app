After the installation, use a terminal to cwd into /app/pkg/cloudron and run `/usr/local/bin/gosu cloudron:cloudron yarn build` (make sure you have enough RAM available (2,5GB+)), then create an admin user.

Email is not working yet, not familiar with nodemailer but could be an easy fix.

LDAP support is on the roadmap but not before 2021 (https://github.com/strapi/strapi/issues/2312)
