module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'postgres',
        host: env('DATABASE_HOST', 'CLOUDRON_POSTGRESQL_HOST'),
        port: env.int('DATABASE_PORT', 'CLOUDRON_POSTGRESQL_PORT'),
        database: env('DATABASE_NAME', 'CLOUDRON_POSTGRESQL_DATABASE'),
        username: env('DATABASE_USERNAME', 'CLOUDRON_POSTGRESQL_USERNAME'),
        password: env('DATABASE_PASSWORD', 'CLOUDRON_POSTGRESQL_PASSWORD'),
        schema: 'public',
      },
      options: {},
    },
  },
});
