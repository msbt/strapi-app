#!/bin/bash

set -eu

if [[ ! -f /app/data/config/server.js ]]; then
	mkdir -p /run/strapi /run/cache /run/ycache /run/yarn /run/yarnrc  /app/data/extensions /app/data/plugin /app/data/config /app/data/uploads /app/data/api /app/data/build /app/data/exports /app/data/components /app/data/cache /app/data/yarn
	cp -r /app/pkg/cloudron/config_orig/* /app/data/config
fi

echo "Updating database config file"
sed -e "s,CLOUDRON_POSTGRESQL_HOST,${CLOUDRON_POSTGRESQL_HOST}," \
    -e "s/'CLOUDRON_POSTGRESQL_PORT'/${CLOUDRON_POSTGRESQL_PORT}/" \
    -e "s/CLOUDRON_POSTGRESQL_DATABASE/${CLOUDRON_POSTGRESQL_DATABASE}/" \
    -e "s/CLOUDRON_POSTGRESQL_USERNAME/${CLOUDRON_POSTGRESQL_USERNAME}/" \
    -e "s/CLOUDRON_POSTGRESQL_PASSWORD/${CLOUDRON_POSTGRESQL_PASSWORD}/" \
    /app/pkg/database.js > /app/data/config/database.js

echo "Setting up email"
sed -e "s,CLOUDRON_MAIL_SMTP_SERVER,${CLOUDRON_MAIL_SMTP_SERVER}," \
    -e "s/'CLOUDRON_MAIL_SMTP_PORT'/${CLOUDRON_MAIL_SMTP_PORT}/" \
    -e "s/CLOUDRON_MAIL_SMTP_USERNAME/${CLOUDRON_MAIL_SMTP_USERNAME}/" \
    -e "s/CLOUDRON_MAIL_SMTP_PASSWORD/${CLOUDRON_MAIL_SMTP_PASSWORD}/" \
    -e "s/CLOUDRON_MAIL_FROM/${CLOUDRON_MAIL_FROM}/" \
    /app/pkg/plugins.js > /app/data/config/plugins.js


echo "Changing ownership"
chown -R cloudron:cloudron /app/data /run/




echo "Starting Strapi"
cd /app/pkg/cloudron
exec /usr/local/bin/gosu cloudron:cloudron yarn develop
